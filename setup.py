from setuptools import setup, find_packages

setup(
    name='openitsetawsaccesskey',
    version='0.1.0',
    packages=find_packages(),
    install_requires=['argparse'],
    entry_points={
        'console_scripts': ['openit_setawsaccesskey=openitsetawsaccesskey.__init__:main']
    }
)
