from pathlib import Path
import argparse
import json

def main():
    args      = get_arguments()
    home      = Path.home()
    directory = '.awsaccesskey'
    filename  = 'awsaccesskey.json'
    filepath  = home / directory / filename
    content   = dict(id=args.id, secret=args.secret)
    filepath.parent.mkdir(parents=True, exist_ok=True)
    with open(filepath, 'w') as file:
        file.write(json.dumps(content, indent=4))

def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('id',     help='Access key id')
    parser.add_argument('secret', help='Access key secret')
    return parser.parse_args()

if __name__ == '__main__':
    main()
