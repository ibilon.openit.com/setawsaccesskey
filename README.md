# setawsaccesskey

Install

```
pip install git+https://gitlab.com/ibilon.openit.com/setawsaccesskey.git
```

Use

```
openit_setawsaccesskey <id> <secret>
```
